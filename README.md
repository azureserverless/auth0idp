# Using Serverless Azure Functions with Auth0 and Google APIs

Code for (blog post)[http://azureserverless.com/2017/03/15/using-serverless-azure-functions-with-auth0-and-google] 
showing how to use Azure Functions with Auth0 for access to google APIs. See the
post for full details

> As a user, I want to log into the app with my Google account so I get a list of my Google Photos albums.

The backend stack is Nodejs on Azure Functions calling Auth0 and the 
Google Photos API via the request module. The client side is a plain old HTML +
JavaScript app using the Auth0 Lock for user login in.

## Running the code

A basic manual installation is as follows:

For a local client development server, I simply installed npm package 'lite-server' 
configured to port 8000 with a ‘bs-config.json’ file.

For the backend, you'll need to create an HTTP Azure Function with the method set
to GET. You'll also need to install the two npm dependencies of 'express-jwt' 
and 'request'. In the Azure Functions control panel go to "Functions App Settings" -> "Console"
to open up a console. Then cd to the folder for your function and enter the following command:

```bash
npm install express-jwt request
```

You'll also need to set up CORS by adding your client URL - eg. localhost:8000. 
This is found in the Azure Functions console panel and click on 
"Function app settings" -> "Configure CORS". Finally, copy the Function’s URL 
into the SPA code constants block.