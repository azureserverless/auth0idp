// constants 
const AUTH0_DOMAIN_URL = 'https://DOMAIN.auth0.com'
const AUTH0_API_ID = 'https://API_ID'
const AUTH0_SIGNING_CERTIFICATE = `-----BEGIN CERTIFICATE-----
<Get this from the Auth0 client Advanced settings -> certificates>
-----END CERTIFICATE-----`
const AUTH0_ADMIN_CLIENT_ID = 'YOUR ADMIN CLIENT APP ID'
const AUTH0_ADMIN_CLIENT_SECRET = 'YOUR ADMIN APP CLIENT SECRET'


// Create decorator that checks the JWT signature and specified fields
const jwtValidateDecorator = require('./azure-functions-auth0')({
  clientId: AUTH0_API_ID,
  clientSecret: AUTH0_SIGNING_CERTIFICATE,
  algorithms: ['RS256'],
  domain: `${AUTH0_DOMAIN_URL}/`
})

// The main Functions Function
module.exports = jwtValidateDecorator((context, req) => {
    if (req.user) {
        // Get a token to access the admin API
        getAdminAccessToken()
        .then(({object: {access_token}}) => {
            const userId = req.user.sub     // has been added to the req by the decorator
            return getUserProfile(access_token, userId)
        })
        // Get the album list from google
        .then(({object}) => {
            const google_access_token = object.identities[0].access_token  // hidden from the Auth0 console
            return getAlbums(google_access_token)
        })
        // Get the album titles
        .then(({object: {feed: {entry}}}) => {
            const titles = entry.map(ent => ent.title.$t)   
            return {
                status: 200,
                body: JSON.stringify(titles),
                headers: {'Content-Type': 'application/json'}
            }
        })
        .catch(err => {
            return {
                status: 400,
                body: err.message
            }
        })
        .then(res => {   
            context.done(null, res)
        })
    }
    else {
        const res = {
            status: 400,
            body: 'Something is wrong with the Authorization token'
        }
        context.done(null, res)
    }
})


// Call a remote HTTP endpoint and return a JSON object
function requestObject(options) {
    return new Promise((resolve, reject) => {
        request(options, function (error, response, body) {
            if (error) {
                reject(error);
            } else if ((200 > response.statusCode) || (299 < response.statusCode)) {
                reject(new Error(`Remote resource ${options.url} returned status code: ${response.statusCode}: ${body}`))
            } else {
                const object = (typeof body === 'string') ? JSON.parse(body) : body // FIXME throws
                resolve({code: response.statusCode, object})
            }
        })
    })
}

// Get an access token for the Auth0 Admin API
function getAdminAccessToken() {
    const options = {
        method: 'POST',
        url: `${AUTH0_DOMAIN_URL}/oauth/token`,
        headers: { 'content-type': 'application/json' },
        body: { client_id: AUTH0_ADMIN_CLIENT_ID,
            client_secret: AUTH0_ADMIN_CLIENT_SECRET,
            audience: `${AUTH0_DOMAIN_URL}/api/v2/`,
            grant_type: 'client_credentials'
        },
        json: true
    }
    return requestObject(options)
}

// Get the user's profile from the Admin API
function getUserProfile(accessToken, userID) {
  const options = {
    method: 'GET',
    url: `${AUTH0_DOMAIN_URL}/api/v2/users/${userID}`,
    headers: { 'Authorization': `Bearer ${accessToken}`
             }
    }
    return requestObject(options)
}

// Get user Google Photos album list
function getAlbums(accessToken) {
    const options = {
        method: 'GET',
        //url: `https://www.googleapis.com/gmail/v1/users/me/labels`,
        url: 'https://picasaweb.google.com/data/feed/api/user/default?alt=json',
        headers: { 'Authorization': `Bearer ${accessToken}`
        }
    }
    return requestObject(options)
}